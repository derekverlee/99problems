;; Find the last box of a list

(defun my-last (lst)
    (if (cdr lst)
	(my-last (cdr lst))
	(car lst)))


 ;; Find the last but one box of a list


; this solultion appears to work, until you try it with a list of just
; one item

(defun my-second-last-broken (lst)
  (if (null (cddr lst))
      (car lst)
      (my-second-last (cdr lst))))

(defun my-second-last (lst)
  (if (cddr lst)
      (my-second-last (cdr lst))
      (and (cdr lst) (car lst))))

;; Problem 3 
;; Find the K'th element of a list
;; The first position is 1

(defun nth-element (lst K)
  (if (> K 1)
      (nth-element (cdr lst) (+ K -1))
      (car lst)))
      

;; Problem 4
; count items in list

(defun my-length (lst)
  (if lst
      (+ 1 
	 (my-length (cdr lst)))
      0))

;; Problem 5
; reverse a list


(defun my-reverse1 (lst)
  (if lst
      (append 
       (my-reverse (cdr lst)) 
       (list (car lst)))))

;; with an accumulator
(defun my-reverse2 (lst &optional (acc '()))
  (if lst
      (my-reverse2 (cdr lst) 
		   (cons (car lst) acc))
      acc))

; a third way involving keeping a refernce to the last cons
; of the reversed list as well as the first?
; perhaps with multiple value bind
; and/or reversing direction of cons association on the return pass


(setf my-reverse #'my-reverse2)

;; Problem 6
;       discover palindrome
(defun is-palindrome (lst)
  (equal lst (my-reverse lst)))


;; Problem 7
; Flatten a nested list structure
(defun my-flatten (X)
  (if X
      (if (listp X)
	  (if (listp (car X))
	     (append (my-flatten (car X)) (my-flatten (cdr X)))
	     (cons (car X) (my-flatten (cdr X))))
	  X)))
	    
;; Problem 8 
; Eliminate consecutive duplicates of list elements.
(defun compress (X) 
  (if X 
      (if (equal (car X) (cadr X))
	  (compress (cdr X))
	  (cons (car X) (compress (cdr X))))))

;; Problem 9
; Pack consecutive duplicates of list elements into sublists
(defun pack (X)
  (if X 
      (let ((f (car X)))
	(if (listp f)
	    (if (equal (car f) (cadr X))
		(let ((p (cons (cadr X) f)))
		  (pack (cons p (cddr X))))
		(cons f (pack (cdr X))))
	    (pack (cons (cons f '()) (cdr X)))))))
	    
; (there has to be a cleaner way)

;; Problem 10 
; Run-length encoding of a list

; lazy way
(defun rle-encode (X)
    (mapcar (lambda (lst)
	      `(,(my-length lst) ,(car lst)))
	    (pack X)))
 
;; Problem 11
; lazy way
(defun rle-encode-modified (X)
    (mapcar (lambda (lst)
		   (if (equal 1 (car lst))
		       (cadr lst)
		       lst))
	    (rle-encode X)))

;; Problem 12 
(defun rle-decode (input)
  (if input
      (let ((f (car input))
	    (r (rle-decode (cdr input)))) 
	 (if (listp f)
	     (append 
	      (loop repeat (car f) collect (cadr f))
	      r)
	     (cons f r)))))
	 

;; Problem 13
; ?


;; Problem 14
(defun duplicate-2 (input)
  (if input
      (cons (car input)
	    (cons 
	     (car input)
	     (duplicate-2 (cdr input))))))


;; Problem 15
;; input a list and an number, output a list wich each element repeating n times
;; eg 
;;  (repli '(a b c) 3) 
;;  (A A A B B B C C C)

(defun repli (lst n)
    (loop 
       for x in lst 
       append 
	 (loop 
	    repeat n 
	    collect x)))

(defun repli-recursive (lst n)
  (if (and lst (> n 0))
      (append (list (car lst)) (repli (list (car lst)) (- n 1) )
	    (repli (cdr lst) n))))

(defun repli-faster (lst n)
  (let (result)
;;    (declare (fixnum n) (optimize (safety 0) (speed 3)))
    (loop for item in lst do 
	 (loop repeat n do
	      (setf result (cons item result))))
    result))

;; Problem 16
;;Drop every N'th element from a list.
;;    Example:
;;    * 
;;    (A B D E G H K)
(defun drop (lst n)
    (loop 
       for x in lst 
       for i from 1
       if 
	 (not (zerop (mod i n))) 
       collect x))


;; Problem 17
;;(*) Split a list into two parts; the length of the first part is given.
;;    Do not use any predefined predicates.

;;    Example:
;;    * (split '(a b c d e f g h i k) 3)
;;    ( (A B C) (D E F G H I K))

(defun split (lst n)
  (loop 
     for i from 1 upto n
     for x on lst
       collect (car x) into head
       finally (if head 
		   (return `(,head ,(cdr x)))
		   (return `(nil ,x)))))
       

(defun split-recursive (lst n &optional head)
       (if (and lst (> n 0))
	   (split-recursive (cdr lst) (- n 1) (append head (list (car lst))))
	   `(,head ,lst)))

;; Problem 18
;; Extract a slice from a list.
;;    Given two indices, I and K, the slice is the list containing the elements between the I'th and K'th element of the original list (both limits included). Start counting the elements with 1.
;;    Example:
;;    * (slice '(a b c d e f g h i k) 3 7)
;;    (C D E F G)

(defun slice (lst I K)
  (if (> I 0)
      (slice (cdr lst) (- I 1) (- K 1))
      (if (> K 0)
	  (reverse (slice (reverse lst) (- (length lst) K) 0))
	  lst)))

;; Problem 19 
;; Rotate a list N places to the left.
;;    Examples:
;;    * (rotate '(a b c d e f g h) 3)
;;    (D E F G H A B C)

;;    * (rotate '(a b c d e f g h) -2)
;;    (G H A B C D E F)

;;    Hint: Use the predefined functions length and append, as well as the result of problem P17.

(defun rotate (lst n)
  (let ((sp (split lst (mod n (my-length lst)))))
    (append (cadr sp) (car sp))))

;;P20 (*) Remove the K'th element from a list.
;;    Example:
;;    * (remove-at '(a b c d) 2)
;;    (A C D)

(defun remove-at (lst n)
  (if (> n 0)
      (let ((sp (split lst (- n 1))))
	(append (car sp) (cdadr sp)))
      lst))
 
;;P21 (*) Insert an element at a given position into a list.
;;    Example:
;;    * (insert-at 'alfa '(a b c d) 2)
;;    (A ALFA B C D)

;; note about implementation
;; for n 0 or less, is the same as 1
;; for (> n (length lst)), inserts after last
(defun insert-at (lst n element)
  (let ((sp (split lst (- n 1))))
    (append (car sp) (list element) (cadr sp))))

;; P22 (*) Create a list containing all integers within a given range.
;;    If first argument is smaller than second, produce a list in decreasing order.
;;    Example:
;;    * (range 4 9)
;;    (4 5 6 7 8 9)

(defun range (start stop) 
  (loop for i from start to stop collect i))